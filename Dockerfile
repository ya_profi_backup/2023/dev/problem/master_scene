ARG BASE_IMG

FROM ${BASE_IMG}
LABEL org.opencontainers.image.authors="texnoman@itmo.com"

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

ENV GUI false

RUN cd src && git clone https://gitlab.com/beerlab/models.git
RUN cd src/ceiling_arm_description && git pull origin master
RUN cd src/hook_gripper_gazebo && git pull origin master

COPY . src/master_problem_scene

RUN cd src/master_problem_scene && catkin build

HEALTHCHECK --interval=20s --timeout=1s --retries=3 --start-period=20s CMD if [[ $(rostopic list 2> /dev/null | wc -c) > 0 ]]; then exit 0; fi;
CMD ["/bin/bash", "-ci", "roslaunch master_problem_scene start_scene.launch gui:=${GUI}"]