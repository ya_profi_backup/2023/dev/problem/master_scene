
![scene pic](docs/figures/scene_view.png)

# master_problem_scene
Репозиторий с файлами сцены для полуфинала олимпиады "Я - профессионал" для магистров.

## [docker] How to use

#### Установка

    docker login registry.gitlab.com -u <username> -p <token>
    docker build --pull -t scene_master_semifinal . --build-arg  BASE_IMG=registry.gitlab.com/beerlab/iprofi2023/problem/master_problem_2023/base:ubuntu-latest

***Note!** To use nvidia set image to base:nvidia-latest*

#### Запуск

    xhost +local:docker
    docker run -ti scene_master_semifinal:latest

## [host] How to use

#### Установка

```bash
mkdir -p ~/rosws/src && cd ~/rosws/src
git clone https://gitlab.com/beerlab/iprofi2023_dev/problem/master_scene.git master_problem_scene

cd ~/rosws
vcs import src < src/master_problem_scene/requirements.noetic.repos

ROS_DISTRO=noetic || rosdep install --from-paths src --ignore-src -y

catkin build
```

#### Запуск

```bash
source ~/rosws/devel/setup.bash
roslaunch master_problem_scene start_scene.launch gui:=true
```

## License
MIT License
